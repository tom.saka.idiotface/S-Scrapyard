# The Instant Calculator
This program provides the easy calculation system.
This code is still tiny, but it will be upgraded.
Have a fun for begginers :)

## Compile (MSYS2 Recommended)
1. Clone this repos.
2. Make.

## Usage
Execute "inst-calc(.exe)" with these options:
    
    1. -a (--add)
    2. -s (--sub)
    3. -m (--multiply)
    4. -d (--divide)
    5. -cmp-b (--biggest)
    6. -cmp-s (--smallest)
    7. -v (--version): Show version informations.

# WARNING
The first commit is not working. It requires fixing pointers.
