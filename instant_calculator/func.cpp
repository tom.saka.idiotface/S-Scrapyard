// ToDo: Reimplement with std::vector.
#include <vector>
double add(std::vector<double> in){
    double calc = 0.0;
    for(int i = 0; i < in.size() ; i++){
        calc = calc + in[i];
    }
    return calc;
}

double del(std::vector<double> in){
    double calc = in[0];
    for(int i = 1; i < in.size(); i++){
        calc = calc - in[i];
    }
    return calc;
}

double multiply(std::vector<double> in){
    double calc = in[0];
    for(int i = 1; i < in.size(); i++){
        calc = calc * in[i];
    }
    return calc;
}

double divide(std::vector<double> in){
    double calc = in[0];
    for(int i = 1; i < in.size(); i++){
        if(in[i] == 0){
            calc = 0.0;
            break;
        }
        calc = calc / in[i];
    }
    return calc;
}

double biggest(std::vector<double> in){
    double biggest = in[0];
    for(int i = 1; i < in.size(); i++){
        if (biggest < in[i]){
            biggest = in[i];
        }
    }
    return biggest;
}

double smallest(std::vector<double> in){
    double smallest = in[0];
    for(int i = 1; i < in.size(); i++){
        if(smallest > in[i]){
            smallest = in[i];
        }
    }
    return smallest;
}

