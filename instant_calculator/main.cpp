#include <iostream>
#include <utility>
#include <vector>
#include <string.h>
#include "func.hpp"

void version(){
    std::cout << "The Instant Calcurator v0.3" << "\n" << "Copyright 2024~ Tomoaki Sakamoto All rights reserved." << "\n\n";
    #ifdef DEBUG_
    std::cout << "NOTE: This program is built with debug mode. To build with release mode, try 'make build' again.\n\n";
    #endif
}

int main(int argc, char* argv[]){
    double (*calc)(std::vector<double>);
    bool FLAG_OPTION_SPECIFIED = false;
    std::vector<double> inputs = {};
    for(int i = 1; i < argc; i++){
        #ifdef DEBUG_
        std::cout << "Loop started\n";
        #endif
        if(strcmp(argv[i],"--add")==0||strcmp(argv[i],"-a")==0){
            #ifdef DEBUG_
            std::cout << "NOTE: add selected.\n";
            #endif
            calc = add;
            FLAG_OPTION_SPECIFIED = true;
        }
        
        else if(strcmp(argv[i],"--sub")==0||strcmp(argv[i],"-s")==0){
            #ifdef DEBUG_
            std::cout << "NOTE: sub selected.\n";
            #endif
            calc = del;
            FLAG_OPTION_SPECIFIED = true;
        }
        
        else if(strcmp(argv[i],"--multiply")==0||strcmp(argv[i],"-m")==0){
            #ifdef DEBUG_
            std::cout << "NOTE: multi selected.\n";
            #endif
            calc = multiply;
            FLAG_OPTION_SPECIFIED = true;
        }
        
        else if(strcmp(argv[i],"--divide")==0||strcmp(argv[i],"-d")==0){
            #ifdef DEBUG_
            std::cout << "NOTE: divide selected.\n";
            #endif
            calc = divide;
            FLAG_OPTION_SPECIFIED = true;
        }
        
        else if(strcmp(argv[i],"--biggest")==0||strcmp(argv[i],"-cmp-b")==0){
            #ifdef DEBUG_
            std::cout << "NOTE: cmp-b selected.\n";
            #endif
            calc = biggest;
            FLAG_OPTION_SPECIFIED = true;
        }
        
        else if(strcmp(argv[i],"--smallest")==0||strcmp(argv[i],"-cmp-s")==0){
            #ifdef DEBUG_
            std::cout << "NOTE: cmp-s selected.\n";
            #endif
            calc = smallest;
            FLAG_OPTION_SPECIFIED = true;
        }
        
        else if(strcmp(argv[i],"--version")==0||strcmp(argv[i],"-v")==0){
            version();
            std::exit(0);
        }

        else{
            inputs.emplace_back(strtof(argv[i],nullptr));
            }
    }
    if(!FLAG_OPTION_SPECIFIED){
        std::cout << "Usage: inst-calc [command] numberA numberB\n\n" 
        << "-a(--add):Add 2 arguments.\n"
        << "-s(--sub):Subtract 2 arguments.\n"
        << "-m(--multiply):Multiply 2 arguments.\n"
        << "-d(--divide):Divide 2 arguments.\n"
        << "-cmp-b(--biggest):Compare which is bigger.\n"
        << "-cmp-s(--smallest):Compare which is smaller.\n"
        << "\n-v(--version):Show copyrights and information of versions.\n";
    }else{
    std::cout << calc(inputs) << "\n";
    return 0;
    }
}